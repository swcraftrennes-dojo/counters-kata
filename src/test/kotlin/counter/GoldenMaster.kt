package counter

import tester.TestCounterPrivate

/**
 * <p>GoldenMaster</p>
 */
fun main() {
    val c:Counter = CounterImpl()
    TestCounterPrivate.check(c)
}
